function [MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor]=InitializeBlocking(Node,Element)

IDZmax=find(Node(:,3)==max(Node(:,3)));
IDZmin= find(Node(:,3)==min(Node(:,3)));

ForceBot= find(and(Node(:,2)==0.0595,Node(:,3)==0));  
IDForceBot=2*ForceBot;

ForceTop= find(and(Node(:,2)==0.0595,Node(:,3)==max(Node(:,3))));  
IDForceTop=2*ForceTop;

MasterForceDOF=[IDForceBot;IDForceTop];
MasterForceDOF=sort(MasterForceDOF,'ascend');

IDZmid=find(and(Node(:,3)==0.5*max(Node(:,3)),Node(:,2)==0));
SlaveDOF=2*IDZmid;

BCTop=find(and(Node(:,2)==0.1785,Node(:,3)==max(Node(:,3))));
BCBot=find(and(Node(:,2)==0.1785,Node(:,3)==0));

BCVert=sort(2*[BCTop;BCBot],'ascend');

BCHor=sort(2*find(Node(:,2)==0)-1,'ascend');

Temp=cat(1,MasterForceDOF,SlaveDOF);
MasterUsualDOF=zeros(2*size(Node,1)-length(MasterForceDOF)-length(SlaveDOF)-length(BCVert)-length(BCHor),1);
counter=0;
UFreeId=cat(1,MasterForceDOF,SlaveDOF,BCVert,BCHor);
n=length(UFreeId);

for inode=1:size(Node,1)
    
    TempVect1=repmat(2*Node(inode,1)-1,n,1);
    TempVect2=repmat(2*Node(inode,1),n,1);
    
    TempRes1=(TempVect1==UFreeId);
    TempRes2=(TempVect2==UFreeId);
    
    if sum(TempRes1==0)==n
        counter=counter+1;
        MasterUsualDOF(counter,1)=2*Node(inode,1)-1;
    end
    
    if sum(TempRes2==0)==n
        counter=counter+1;
        MasterUsualDOF(counter,1)=2*Node(inode,1);
    end
    
end

if sum(MasterUsualDOF==0)~=0
    
    disp('Error in Blocking DOF')
    
end

