function [TempElement]=DamageUpdating(Node,Element,iTime,UTry,PForceGaussSeries,PForceGaussInf,OldGaussSeries,OldGaussInf,E,Rho,Einf,v,a,b,alpha)

disp('Entering into DamageUpdating');
TempElement=Element;

D=(1/((1+v)*(1-2*v)))*[1-v v 0;v 1-v 0;0 0 1-2*v]; % Plain Strain

TimeStart=RTime(iTime-1);
TimeEnd=RTime(iTime);
TimeMiddle=0.5*(TimeStart+TimeEnd);
DeltaRTime=TimeEnd-TimeStart;

[~,~,~,~,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y,B4_x,B4_y]=ShapeFunctionBuilder();

TPForceGaussSeries=zeros(12*size(Element,1),size(Rho,1));

for ielem=1:size(Element,1)
    
    
    %     ElementId=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4);2*Element(ielem,5)-1;2*Element(ielem,5)];
    
    Uelem1=UTry(ElementId,1);
    Uelem2=UTry(ElementId,2);
    DeltaU=Uelem2-Uelem1;
    
    TempNode=zeros(4,3);
    TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
    TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
    TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
    TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
    
    a=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
    b=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
    
    
    for igpt=1:4
        
        dN1_x=feval(B1_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN1_y=feval(B1_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN2_x=feval(B2_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN2_y=feval(B2_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN3_x=feval(B3_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN3_y=feval(B3_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN4_x=feval(B4_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN4_y=feval(B4_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        
        B=zeros(3,8);
        
        
        
        B(1,1)=dN1_x;B(2,2)=dN1_y;B(3,1)=dN1_y;B(3,2)=dN1_x;
        B(1,3)=dN2_x;B(2,4)=dN2_y;B(3,3)=dN2_y;B(3,4)=dN2_x;
        B(1,5)=dN3_x;B(2,6)=dN3_y;B(3,5)=dN3_y;B(3,6)=dN3_x;
        B(1,7)=dN4_x;B(2,8)=dN4_y;B(3,7)=dN4_y;B(3,8)=dN4_x;
        
        index1=12*(ielem-1)+3*(igpt-1)+1;
        index2=12*(ielem-1)+3*(igpt-1)+3;
        
        for iprony=1:size(E,1)
            
            EtPart=PronyIntegPart(E(iprony),Rho(iprony),TimeMiddle,TimeEnd);
            TPForceGaussSeries(index1:index2,iprony)=D*EtPart*B*DeltaU;
            
        end
        
        TPForceGaussInf(index1:index2,1)=D*Einf*B*Uelem2;
        PForceGaussSeries(index1:index2,:)=TPForceGaussSeries(index1:index2,:)+PForceGaussSeries(index1:index2,:);
        PForceGaussInf(index1:index2,:)=TPForceGaussInf(index1:index2,:)+PForceGaussInf(index1:index2,:);
        
        TempOldGaussStress=sum(OldGaussSeries(index1:index2,:),2)+OldGaussInf(index1:index2,1);
        TempPlainStress=v*(TempOldGaussStress(1)+TempOldGaussStress(2));
        OldStress=[TempOldGaussStress(1) TempOldGaussStress(3) 0;TempOldGaussStress(3) TempOldGaussStress(2) 0;0 0 TempPlainStress];
        
        TempNewStress=sum(PForceGaussSeries(index1:index2,:),2)+PForceGaussInf(index1:index2,1);
        TempPlainStress=v*(TempNewStress(1)+TempNewStress(2));
        NewStress=[TempNewStress(1) TempNewStress(3) 0;TempNewStress(3) TempNewStress(2) 0;0 0 TempPlainStress];
        
        [~,DiagOld]=eig(OldStress);
        [~,DiagNew]=eig(NewStress);
        
        E33Old=max(diag(DiagOld));EvOld=sum(diag(DiagOld));EdOld=E33Old-(1/3)*EvOld;
        E33New=max(diag(DiagNew));EvNew=sum(diag(DiagNew));EdNew=E33Old-(1/3)*EvNew;
        
        if E33New > 0
            
            C0=Element(ielem,igpt+5);
            S0=(-1*Log(C0)/a)^(1/b);
            
            Gt=((1/3)*EvOld+EdOld)^(2*alpha);
            Gtdt=((1/3)*EvNew+EdNew)^(2*alpha);
            
            Ft=((S0^(bc-1))*C0)^alpha;
            dFds=(C0*((b-1)*S0^(b-2)-a*b*S0^(2*b-2)))*alpha*((S0^(b-1))*C0)^(alpha-1);
            
            Landa=(0.5*a*b)^alpha;
            RH=DeltaRTime*Landa*0.5*ft*(gt+gtdt);
            LH=1-0.5*DeltaRTime*Landa*dfds*gtdt;
            DeltaS=RH/LH;
            
            if or(or(dS < 0, isinf(dS)),isnan(dS))
                
                disp('Error in damage updating')
                
            else
                
                Sn=S0+dS;
                Cn=exp(-1*a*(Sn^b));
                TempElement(ielem,5+igpt)=Cn;
                
                
                
            end
            
        end
        
    end
    
end
    
