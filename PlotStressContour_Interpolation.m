function [NodalSigmaXX,NodalSigmaYY,NodalSigmaXY,NodalC] = PlotStressContour_Interpolation(Node,Element,StressGaussTime,CHistory)


clc
n=size(StressGaussTime,2);
nn=size(CHistory,2);

NodalSigmaXX=zeros(size(Node,1),n);
NodalSigmaYY=zeros(size(Node,1),n);
NodalSigmaXY=zeros(size(Node,1),n);
NodalC=zeros(size(Node,1),nn);

NodeData=zeros(size(Node,1),1);

% row=size(Element,1);
% col=size(Element,2);

% TNodal=reshape(Element(:,2:5),row*4);

for ielem=1:size(Element,1)
    
    igpt=1;
    pt1=12*(ielem-1)+3*(igpt-1)+1;
    igpt=2;
    pt2=12*(ielem-1)+3*(igpt-1)+1;
    igpt=3;
    pt3=12*(ielem-1)+3*(igpt-1)+1;
    igpt=4;
    pt4=12*(ielem-1)+3*(igpt-1)+1;
    
    pt=cat(1,pt1,pt2,pt3,pt4);
    
    SigmaXX=StressGaussTime(pt,:);
    
    x=[-1/sqrt(3)  1/sqrt(3) 1/sqrt(3) -1/sqrt(3)];
    y=[-1/sqrt(3) -1/sqrt(3) 1/sqrt(3)  1/sqrt(3)];
    
    A=zeros(3,3);
    A(1,1)=sum(x.^2);A(1,2)=sum(x.*y);A(1,3)=sum(x);
    A(2,1)=A(1,2);A(2,2)=sum(y.^2);A(2,3)=sum(y);
    A(3,1)=A(1,3);A(3,2)=sum(y);A(3,3)=4;
    
    B=zeros(3,n);
    
    B(1,:)=sum(bsxfun(@times,x',SigmaXX),1);
    B(2,:)=sum(bsxfun(@times,y',SigmaXX),1);
    B(3,:)=sum(bsxfun(@times,ones(4,1),SigmaXX));
    
    Cof=A\B;
    
    NodalSigmaXX(Node(Element(ielem,2),1),:)=NodalSigmaXX(Node(Element(ielem,2),1),:)+(Cof'*[-1;-1;1])';
    NodalSigmaXX(Node(Element(ielem,3),1),:)=NodalSigmaXX(Node(Element(ielem,3),1),:)+(Cof'*[+1;-1;1])';
    NodalSigmaXX(Node(Element(ielem,4),1),:)=NodalSigmaXX(Node(Element(ielem,4),1),:)+(Cof'*[+1;+1;1])';
    NodalSigmaXX(Node(Element(ielem,5),1),:)=NodalSigmaXX(Node(Element(ielem,5),1),:)+(Cof'*[-1;+1;1])';
    
    igpt=1;
    pt1=12*(ielem-1)+3*(igpt-1)+2;
    igpt=2;
    pt2=12*(ielem-1)+3*(igpt-1)+2;
    igpt=3;
    pt3=12*(ielem-1)+3*(igpt-1)+2;
    igpt=4;
    pt4=12*(ielem-1)+3*(igpt-1)+2;
    
    pt=cat(1,pt1,pt2,pt3,pt4);
    
    SigmaYY=StressGaussTime(pt,:);
    
    x=[-1/sqrt(3)  1/sqrt(3) 1/sqrt(3) -1/sqrt(3)];
    y=[-1/sqrt(3) -1/sqrt(3) 1/sqrt(3)  1/sqrt(3)];
    
    A=zeros(3,3);
    A(1,1)=sum(x.^2);A(1,2)=sum(x.*y);A(1,3)=sum(x);
    A(2,1)=A(1,2);A(2,2)=sum(y.^2);A(2,3)=sum(y);
    A(3,1)=A(1,3);A(3,2)=sum(y);A(3,3)=4;
    
    B=zeros(3,n);
    
    B(1,:)=sum(bsxfun(@times,x',SigmaYY),1);
    B(2,:)=sum(bsxfun(@times,y',SigmaYY),1);
    B(3,:)=sum(bsxfun(@times,ones(4,1),SigmaYY));
    
    Cof=A\B;
    
    NodalSigmaYY(Node(Element(ielem,2),1),:)=NodalSigmaYY(Node(Element(ielem,2),1),:)+(Cof'*[-1;-1;1])';
    NodalSigmaYY(Node(Element(ielem,3),1),:)=NodalSigmaYY(Node(Element(ielem,3),1),:)+(Cof'*[+1;-1;1])';
    NodalSigmaYY(Node(Element(ielem,4),1),:)=NodalSigmaYY(Node(Element(ielem,4),1),:)+(Cof'*[+1;+1;1])';
    NodalSigmaYY(Node(Element(ielem,5),1),:)=NodalSigmaYY(Node(Element(ielem,5),1),:)+(Cof'*[-1;+1;1])';
    
    igpt=1;
    pt1=12*(ielem-1)+3*(igpt-1)+3;
    igpt=2;
    pt2=12*(ielem-1)+3*(igpt-1)+3;
    igpt=3;
    pt3=12*(ielem-1)+3*(igpt-1)+3;
    igpt=4;
    pt4=12*(ielem-1)+3*(igpt-1)+3;
    
    pt=cat(1,pt1,pt2,pt3,pt4);
    
    SigmaXY=StressGaussTime(pt,:);
    
    x=[-1/sqrt(3)  1/sqrt(3) 1/sqrt(3) -1/sqrt(3)];
    y=[-1/sqrt(3) -1/sqrt(3) 1/sqrt(3)  1/sqrt(3)];
    
    A=zeros(3,3);
    A(1,1)=sum(x.^2);A(1,2)=sum(x.*y);A(1,3)=sum(x);
    A(2,1)=A(1,2);A(2,2)=sum(y.^2);A(2,3)=sum(y);
    A(3,1)=A(1,3);A(3,2)=sum(y);A(3,3)=4;
    
    B=zeros(3,n);
    
    B(1,:)=sum(bsxfun(@times,x',SigmaXY),1);
    B(2,:)=sum(bsxfun(@times,y',SigmaXY),1);
    B(3,:)=sum(bsxfun(@times,ones(4,1),SigmaXY));
    
    Cof=A\B;
    
    
    NodalSigmaXY(Node(Element(ielem,2),1),:)=NodalSigmaXY(Node(Element(ielem,2),1),:)+(Cof'*[-1;-1;1])';
    NodalSigmaXY(Node(Element(ielem,3),1),:)=NodalSigmaXY(Node(Element(ielem,3),1),:)+(Cof'*[+1;-1;1])';
    NodalSigmaXY(Node(Element(ielem,4),1),:)=NodalSigmaXY(Node(Element(ielem,4),1),:)+(Cof'*[+1;+1;1])';
    NodalSigmaXY(Node(Element(ielem,5),1),:)=NodalSigmaXY(Node(Element(ielem,5),1),:)+(Cof'*[-1;+1;1])';
    
    
    pt1=4*(ielem-1)+1;
    pt2=4*(ielem-1)+2;
    pt3=4*(ielem-1)+3;
    pt4=4*(ielem-1)+4;
    
    pt=cat(1,pt1,pt2,pt3,pt4);
    
    C=CHistory(pt,:);
    
    x=[-1/sqrt(3)  1/sqrt(3) 1/sqrt(3) -1/sqrt(3)];
    y=[-1/sqrt(3) -1/sqrt(3) 1/sqrt(3)  1/sqrt(3)];
    
    A=zeros(3,3);
    A(1,1)=sum(x.^2);A(1,2)=sum(x.*y);A(1,3)=sum(x);
    A(2,1)=A(1,2);A(2,2)=sum(y.^2);A(2,3)=sum(y);
    A(3,1)=A(1,3);A(3,2)=sum(y);A(3,3)=4;
    
    B=zeros(3,nn);

    B(1,:)=sum(bsxfun(@times,x',C),1);
    B(2,:)=sum(bsxfun(@times,y',C),1);
    B(3,:)=sum(bsxfun(@times,ones(4,1),C));
    
    Cof=A\B;

   
    Data=(Cof'*[-1;-1;1])';
    Data(Data<0.1)=0.1;
    Data(Data>1)=1;
    
    NodalC(Node(Element(ielem,2),1),:)=NodalC(Node(Element(ielem,2),1),:)+Data;
    NodalC(Node(Element(ielem,3),1),:)=NodalC(Node(Element(ielem,3),1),:)+Data;
    NodalC(Node(Element(ielem,4),1),:)=NodalC(Node(Element(ielem,4),1),:)+Data;
    NodalC(Node(Element(ielem,5),1),:)=NodalC(Node(Element(ielem,5),1),:)+Data;
    
    NodeData(Node(Element(ielem,2),1))=NodeData(Node(Element(ielem,2),1))+1;
    NodeData(Node(Element(ielem,3),1))=NodeData(Node(Element(ielem,3),1))+1;
    NodeData(Node(Element(ielem,4),1))=NodeData(Node(Element(ielem,4),1))+1;
    NodeData(Node(Element(ielem,5),1))=NodeData(Node(Element(ielem,5),1))+1;
    
    
end


for iNode=1:size(Node,1)
    
    NodalSigmaXX(iNode,:)=NodalSigmaXX(iNode,:)/NodeData(iNode,1);
    NodalSigmaYY(iNode,:)=NodalSigmaYY(iNode,:)/NodeData(iNode,1);
    NodalSigmaXY(iNode,:)=NodalSigmaXY(iNode,:)/NodeData(iNode,1);
    NodalC(iNode,:)=NodalC(iNode,:)/NodeData(iNode,1);
    
end

