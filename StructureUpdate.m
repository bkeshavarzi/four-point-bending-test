function [DeltaUSol,Element,Direction]=StructureUpdate(Node,TempElement,Element,UTry,F,MasterForceDOF,MasterUsualDOF,SlaveDOF,Direction,PForceGaussSeries,PForceGaussInf,RTime,iTime,E,Rho,Einf,v,ac,bc,alpha)
% Node,TempElement,Element,UIter,FG,MasterForceDOF,MasterUsualDOF,SlaveDOF,TempDirection,PForceGaussSeries,PForceGaussInf,RTime,iTime,E,Rho,Einf,v,a,b,alpha
% Bring Different types of degree of freedom in here

D=(1/((1+v)*(1-2*v)))*[1-v v 0;v 1-v 0;0 0 1-2*v]; % Plain Strain

GPT_x=[-1/sqrt(3)   1/sqrt(3)  1/sqrt(3)  -1/sqrt(3)];
GPT_y=[-1/sqrt(3)  -1/sqrt(3)  1/sqrt(3)   1/sqrt(3)];
WGPT=[1 1 1 1];

TimeStart=RTime(iTime-1);
TimeEnd=RTime(iTime);
TimeMiddle=0.5*(TimeStart+TimeEnd);
DeltaRTime=TimeEnd-TimeStart;

[~,~,~,~,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y,B4_x,B4_y]=ShapeFunctionBuilder();

TPForceGaussSeries=zeros(12*size(Element,1),size(Rho,1));
NPForceGaussSeries=zeros(12*size(Element,1),size(Rho,1));

KG=zeros(2*size(Node,1));

ForceSeries=zeros(2*size(Node,1),size(Rho,1)); % This Matrix Stores force for each Elemnt in current time strep due to viscous Effect_ all the prony is stored
ForceInf=zeros(2*size(Node,1),1);              % This Matrix Stores force for each Elemnt in current time strep due to viscous Effect_ Einf is stored

for ielem=1:size(Element,1)
    
    Kelem=zeros(8);
    ElementId=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4);2*Element(ielem,5)-1;2*Element(ielem,5)];
    
    Uelem1=UTry(ElementId,1);
    Uelem2=UTry(ElementId,2);
    DeltaU=Uelem2-Uelem1;
    
    %DeltaU=UTry(ElementId,1);
    
    TempNode=zeros(4,3);
    TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
    TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
    TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
    TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
    
    a=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
    b=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
    
    
    for igpt=1:4
        
        dN1_x=feval(B1_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN1_y=feval(B1_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN2_x=feval(B2_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN2_y=feval(B2_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN3_x=feval(B3_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN3_y=feval(B3_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN4_x=feval(B4_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN4_y=feval(B4_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        
        B=zeros(3,8);
        
        B(1,1)=dN1_x;B(2,2)=dN1_y;B(3,1)=dN1_y;B(3,2)=dN1_x;
        B(1,3)=dN2_x;B(2,4)=dN2_y;B(3,3)=dN2_y;B(3,4)=dN2_x;
        B(1,5)=dN3_x;B(2,6)=dN3_y;B(3,5)=dN3_y;B(3,6)=dN3_x;
        B(1,7)=dN4_x;B(2,8)=dN4_y;B(3,7)=dN4_y;B(3,8)=dN4_x;
        
        index1=12*(ielem-1)+3*(igpt-1)+1;
        index2=12*(ielem-1)+3*(igpt-1)+3;
        
        for iprony=1:size(E,1)
            
            EtPart=PronyIntegPart(E(iprony),Rho(iprony),TimeMiddle,TimeEnd);
            TPForceGaussSeries(index1:index2,iprony)=D*EtPart*B*DeltaU;
            
        end
        
        C0=TempElement(ielem,igpt+5);
        
        A11=(1/9)*(C0+1*(2*(1+v)/(1-2*v)));
        A22=C0+0.5*((1-2*v)/(1+v));
        A12=(1/3)*(C0-1);
        A44=0.5/(1+v);
        A66=A44;
        
        C=zeros(3);
        
        C(1,1)=A11-(2/3)*A12+(1/9)*A22+A66;
        C(1,2)=A11+(1/3)*A12-(2/9)*A22;
        C(1,3)=0;
        C(2,1)=C(1,2);
        C(2,2)=A11+(4/3)*A12+(4/9)*A22;
        C(2,3)=0;
        C(3,1)=0;
        C(3,2)=0;
        C(3,3)=A44;
        
        NPForceGaussSeries(index1:index2,:)=TPForceGaussSeries(index1:index2,:)+PForceGaussSeries(index1:index2,:); % There is no need to update PForceGauss Series ???
        
        GPStress=sum(NPForceGaussSeries(index1:index2,:),2);
        Sigma=[GPStress(1) GPStress(3);GPStress(3) GPStress(2)];
        [v,d]=eig(Sigma);
        index=find(max(diag(d))==diag(d));
        
        if max(diag(d)) > 0
            
            TTeta=-1*acos(v(:,index)'*[1;0]); % Rethink about Teta direction. I mean + or -
            Direction(4*(ielem-1)+igpt,1)=TTeta;
            
        else
            
            TTeta=Direction(4*(ielem-1)+igpt,1);
            
        end
        
        %       TTeta=Direction(4*(ielem-1)+igpt,1);
        T=[cos(TTeta)^2 sin(TTeta)^2 -2*sin(TTeta)*cos(TTeta);sin(TTeta)^2 cos(TTeta)^2 2*sin(TTeta)*cos(TTeta);1*sin(TTeta)*cos(TTeta) -1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2]; % Check this ????
        
        CBar=inv(T)*C*T;
        
        ElementGaussStress=sum(NPForceGaussSeries(index1:index2,:),2);
        
        NonLinearElementGaussStress=CBar*ElementGaussStress;  % Finding stress tensor for currnet damage state at each gauss point
        
        ElementGaussStressTensor=[NonLinearElementGaussStress(1)  NonLinearElementGaussStresss(3);NonLinearElementGaussStress(3) NonLinearElementGaussStress(2)];
        LinearElementGaussStressTensor=[ElementGaussStress(1) ElementGaussStresss(3);ElementGaussStress(3) ElementGaussStress(2)];
        
        [~,PElementGaussStressTensor]=eig(ElementGaussStressTensor);
        [~,LinearPElementGaussStressTensor]=eig(LinearElementGaussStressTensor);
        
        PElementGaussStressVec=sort(diag(PElementGaussStressTensor),'ascend');
        LinearPElementGaussStressTensor=sort(diag(LinearPElementGaussStressTensor),'ascend');
        
        if PElementGaussStressVec(3) <= 0
            
            % Maximum Principal stress  is negetive, No increase in C
            Cn=Element(ielem,5+igpt); %Cn=new C
            
        else
            
            EpsiolnRVNew=sum(LinearPElementGaussStressTensor);
            EpsilonRDNew=LinearPElementGaussStressTensor(3)-(1/3)*EpsiolnRVNew;
            gtdt=((1/3)*EpsiolnRVNew+EpsilonRDNew)^(2*alpha); %Check this
            
            ElementGaussStressOld=sum(PForceGaussSeries(index1:index2,:),2);
            ElementGaussStressTensorOld=[ElementGaussStressOld(1)  ElementGaussStressOld(3);ElementGaussStressOld(3) ElementGaussStressOld(2)];
            
            [~,PElementGaussStressTensorOld]=eig(ElementGaussStressTensorOld);
            PElementGaussStressTensorOld=sort(diag(PElementGaussStressTensorOld),'ascend');
            
            EpsiolnRVOld=sum(PElementGaussStressTensorOld);
            EpsiolnRDOld=PElementGaussStressTensorOld(3)-(1/3)*EpsiolnRVOld;
            gt=((1/3)*EpsiolnRVOld+EpsiolnRDOld)^(2*alpha);
            
            C0=Element(ielem,5+igpt);
            S0=((-1/ac)*log(C0))^(1/bc);
            
            % Double check this
            dfds=(C0*((bc-1)*S0^(bc-2)-ac*bc*S0^(2*bc-2)))*alpha*((S0^(bc-1))*C0)^(alpha-1);
            Landa=(0.5*ac*bc)^alpha;
            ft=((S0^(bc-1))*C0)^alpha;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            RH=DeltaRTime*Landa*0.5*ft*(gt+gtdt);
            LH=1-0.5*DeltaRTime*Landa*dfds*gtdt;
            
            dS=RH/LH;
            
            if or(or(dS < 0, isinf(dS)),isnan(dS))
                
                disp('Error in damage updating')
                
            else
                
                Sn=S0+dS;
                Cn=exp(-1*as*(Sn^bs));
                TempElement(ielem,5+igpt)=Cn;
                
            end
            
            
        end
        
        A11=(1/9)*(Cn+1*(2*(1+v)/(1-2*v)));
        A22=Cn+0.5*((1-2*v)/(1+v));
        A12=(1/3)*(Cn-1);
        A44=0.5/(1+v);
        A66=A44;
        
        C=zeros(3);
        
        C(1,1)=A11-(2/3)*A12+(1/9)*A22+A66;
        C(1,2)=A11+(1/3)*A12-(2/9)*A22;
        C(1,3)=0;
        C(2,1)=C(1,2);
        C(2,2)=A11+(4/3)*A12+(4/9)*A22;
        C(2,3)=0;
        C(3,1)=0;
        C(3,2)=0;
        C(3,3)=A44;
        
        TTeta=-1*Direction(4*(ielem-1)+igpt,1);
        T=[cos(TTeta)^2 sin(TTeta)^2 -2*sin(TTeta)*cos(TTeta);sin(TTeta)^2 cos(TTeta)^2 2*sin(TTeta)*cos(TTeta);sin(TTeta)*cos(TTeta) -1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2]; % Check this ????
        
        CBar=inv(T)*C*T;
        Kelem=Kelem+B'*CBar*D*EpronyCalculation(TimeEnd,TimeMiddle,Einf,Rho,E)*B*WGPT(igpt)*a*b;
        
        for iprony=1:size(Rho,1)
            
            ForceSeries(ElementId,iprony)=ForceSeries(ElementId,iprony)+B'*CBar*D*EpronyCalculationPart(TimeEnd,TimeMiddle,Rho(iprony),E(iprony))*B*WGPT(igpt)*DeltaU*a*b;
            
        end
        
        ForceInf(ElementId,1)=ForceInf(ElementId,1)+B'*CBar*D*Einf*B*WGPT(igpt)*DeltaU*a*b;
        
    end
    
    for inode=1:4
        
        for jnode=1:4
            
            NodeIdi=Element(ielem,inode+1);
            NodeIdj=Element(ielem,jnode+1);
            
            KG(2*NodeIdi-1,2*NodeIdj-1) =  KG(2*NodeIdi-1,2*NodeIdj-1) +  Kelem(2*inode-1,2*jnode-1);
            KG(2*NodeIdi-1,2*NodeIdj)   =  KG(2*NodeIdi-1,2*NodeIdj)   +  Kelem(2*inode-1,2*jnode);
            KG(2*NodeIdi,2*NodeIdj-1)   =  KG(2*NodeIdi,2*NodeIdj-1)   +  Kelem(2*inode,2*jnode-1);
            KG(2*NodeIdi,2*NodeIdj)     =  KG(2*NodeIdi,2*NodeIdj)     +  Kelem(2*inode,2*jnode);
            
        end
        
    end

    K_mu_mu=KG(MasterUsualDOF,MasterUsualDOF); Km2m2=K_mu_mu;  %Km2m2
    K_mu_mf=KG(MasterUsualDOF,MasterForceDOF); Km2m1=K_mu_mf;  %Km2m1
    K_s_mf=KG(SlaveDOF,MasterForceDOF);        Ksm1=K_s_mf;    %Ksm1
    K_s_s=KG(SlaveDOF,SlaveDOF);               Kss= K_s_s;     %Kss
    K_mu_s=KG(MasterUsualDOF,SlaveDOF);        Km2s=K_mu_s;    %Km2s
    K_mf_mf=KG(MasterForceDOF,MasterForceDOF); Km1m1=K_mf_mf;  %Km1m1
    
    KF=cat(2,Km2m2,Km2m1,zeros(length(MasterUsualDOF),length(SlaveDOF)));
    KF=cat(1,KG,cat(2,Km1m2,Km1m1,-1*ones(length(MasterForceDOF),length(SlaveDOF))));
    KF=cat(1,KG,cat(2,Ksm2,Ksm1,zeros(length(SlaveDof),length(SlaveDof))));
    
    PForceSeries=PForceSeries+ForceSeries;
    PForceInf=PForceInf+ForceInf;
    
    Us=-1*Epsilon0*70*10^-3*abs(sin(w0*(iTime-1)*dT));  %I assume that LVDT length is 70 mm and calculate corresponding dispalacement at slave Node
    
    Force1=-1*K_mu_s*Us;
    Force2=-1*K_mf_s*Us;
    Force3=-1*K_s_s*Us;
    
    nTDOF=length(MasterUsualDOF)+length(MasterForceDOF)+length(SlaveDOF);
    
    ExternalForce=zeros(nTDOF,1);
    ExternalForce(MasterUsualDOF,1)=Force1;
    ExternalForce(MasterForceDOF,1)=Force2;
    ExternalForce(SlaveDOF,1)=Force3;
    
    TempInternalForce=zeros(nTDOF,1);
    
    TempInternalForce(MasterUsualDOF,1)=sum(-1*PForceSeries(MasterUsualDOF,:),2)-PForceInf(MasterUsualDOF);
    TempInternalForce(MasterForceDOF,1)=sum(-1*PForceSeries(MasterForceDOF,:),2)-PForceInf(MasterForceDOF);
    TempInternalForce(SlaveDOF,1)=sum(-1*PForceSeries(SlaveDOF,:),2)-PForceInf(SlaveDOF);
    
    DeltaF=-1*TempInternalForce+ExternalForce;
    
    F_mu=DeltaF(MasterUsualDOF,1);
    F_mf=DeltaF(MasterForceDOF,1);
    F_s=DeltaF(SlaveDOF,1);
    
    
    FG=cat(1,F_mu,F_mf,F_s);

    Sol=KF\FG;

    DeltaU(1:length(MasterUsualDOF))=Sol(1:length(MasterUsualDOF));
    DeltaU(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOf))=Sol(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOf));
    DeltaU(length(MasterUsualDOF)+length(MasterForceDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF)+length(SlaveDof))=-1*Epsilon0*70*10^-3*abs(sin(w0*(iTime-1)*dT));
    
    [~,Index]=sort(cat(1,MasterUsualDOF,MasterForceDOF,SlaveDof),'ascend');
    DeltaUSol=DeltaU(Index);

end

