function [U,Stress,Force]=GStiffnessMatrixCycleIncrement(Node,Element,t,iCycle,U,Direction,DirectionChange,TimeDomainTime,TimeMatrix,v,E,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,Freq,SelectedW,USelectedW,Uwm,Force,Stress)
                                                        
GPT_x=[-1/sqrt(3)   1/sqrt(3)  1/sqrt(3)  -1/sqrt(3)];
GPT_y=[-1/sqrt(3)  -1/sqrt(3)  1/sqrt(3)   1/sqrt(3)];

WGPT=[1 1 1 1];
t=t/1000;

[~,~,~,~,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y,B4_x,B4_y]=ShapeFunctionBuilder();

UW=zeros(2*size(Node,1),length(SelectedW));
FreqForce=zeros(length(SelectedW),1);
D=(E/(1-v^2))*[1 v 0;v 1 0;0 0 1-v];


for iw = 1 : length(SelectedW)
    
    KG=zeros(2*size(Node,1));
    
    for ielem=1:size(Element,1)
        
        Kelem=zeros(8);
        
        TempNode=zeros(4,3);
        TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
        TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
        TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
        TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
        
        aa=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
        bb=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
        
        for igpt=1:4

            dN1_x=feval(B1_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN1_y=feval(B1_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN2_x=feval(B2_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN2_y=feval(B2_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN3_x=feval(B3_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN3_y=feval(B3_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN4_x=feval(B4_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN4_y=feval(B4_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            
            B(1,1)=dN1_x;B(2,2)=dN1_y;B(3,1)=dN1_y;B(3,2)=dN1_x;
            B(1,3)=dN2_x;B(2,4)=dN2_y;B(3,3)=dN2_y;B(3,4)=dN2_x;
            B(1,5)=dN3_x;B(2,6)=dN3_y;B(3,5)=dN3_y;B(3,6)=dN3_x;
            B(1,7)=dN4_x;B(2,8)=dN4_y;B(3,7)=dN4_y;B(3,8)=dN4_x;

            CBar=D;
            Kelem=Kelem+B'*CBar*B*WGPT(igpt)*aa*bb*t;
            
        end
        
        for inode=1:4
            
            for jnode=1:4
                
                NodeIdi=Element(ielem,inode+1);
                NodeIdj=Element(ielem,jnode+1);
                
                KG(2*NodeIdi-1,2*NodeIdj-1) =  KG(2*NodeIdi-1,2*NodeIdj-1) +  Kelem(2*inode-1,2*jnode-1);
                KG(2*NodeIdi-1,2*NodeIdj)   =  KG(2*NodeIdi-1,2*NodeIdj)   +  Kelem(2*inode-1,2*jnode);
                KG(2*NodeIdi,2*NodeIdj-1)   =  KG(2*NodeIdi,2*NodeIdj-1)   +  Kelem(2*inode,2*jnode-1);
                KG(2*NodeIdi,2*NodeIdj)     =  KG(2*NodeIdi,2*NodeIdj)     +  Kelem(2*inode,2*jnode);
                
            end
            
        end
        
    end
    
    MainDof=sort(cat(1,MasterForceDOF,MasterUsualDOF,SlaveDOF),'ascend');
    BC=sort(cat(1,BCVert,BCHor),'descend');
    UnitForce=zeros(size(Node,1)*2,1);
    UnitForce(SlaveDOF,1)=1;
    KG(BC,:)=[];
    KG(:,BC)=[];
    UnitForce(BC)=[];
    TempU=KG\UnitForce;

    UnitLoadU=zeros(2*size(Node,1),1);
    UnitLoadU(MainDof,1)=TempU;
    VirtualUnitEnergy=1;
    UnitEnergy=sum(UnitLoadU(MasterForceDOF));
    EF=VirtualUnitEnergy/UnitEnergy;
    
    ExternalForce=zeros(2*size(Node,1),1);
    ExternalForce(MasterForceDOF,1)=EF;
    ExternalForce(BC)=[];
    
    TempU=KG\ExternalForce;
    UW(MainDof,iw)=TempU;
    
    FreqForce(iw,1)=4*EF;
    
end

UWFull=bsxfun(@times,Uwm,interp1(SelectedW,UW',Freq))';
ForceWFull=bsxfun(@times,Uwm,interp1(SelectedW,FreqForce,Freq));

U(:,length(TimeDomainTime)+(iCycle-2)*length(TimeMatrix)+1:length(TimeDomainTime)+(iCycle-1)*length(TimeMatrix))=real(ifft(ifftshift(UWFull')))';
Force(length(TimeDomainTime)+(iCycle-2)*length(TimeMatrix)+1:length(TimeDomainTime)+(iCycle-1)*length(TimeMatrix))=real(ifft(ifftshift(ForceWFull)));

FStress=zeros(4*3*size(Element,1),length(Freq));

for iw = 1 : length(Freq)
    
    for ielem=1:size(Element,1)
        
        TempNode=zeros(4,3);
        TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
        TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
        TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
        TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
        
        aa=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
        bb=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
        
        ElementId_Force=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4);2*Element(ielem,5)-1;2*Element(ielem,5)];
        
        for igpt=1:4
            
            GaussIdIndex=12*(ielem-1)+3*(igpt-1)+1:12*(ielem-1)+3*(igpt-1)+3;
            
            dN1_x=feval(B1_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN1_y=feval(B1_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN2_x=feval(B2_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN2_y=feval(B2_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN3_x=feval(B3_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN3_y=feval(B3_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            dN4_x=feval(B4_x,GPT_x(igpt),GPT_y(igpt))*(1/aa);
            dN4_y=feval(B4_y,GPT_x(igpt),GPT_y(igpt))*(1/bb);
            
            B(1,1)=dN1_x;B(2,2)=dN1_y;B(3,1)=dN1_y;B(3,2)=dN1_x;
            B(1,3)=dN2_x;B(2,4)=dN2_y;B(3,3)=dN2_y;B(3,4)=dN2_x;
            B(1,5)=dN3_x;B(2,6)=dN3_y;B(3,5)=dN3_y;B(3,6)=dN3_x;
            B(1,7)=dN4_x;B(2,8)=dN4_y;B(3,7)=dN4_y;B(3,8)=dN4_x;

            CBar=D;          
            FStress(GaussIdIndex,iw)=CBar*B*UWFull(ElementId_Force,iw);
            
        end

    end
    
end

Stress(:,(iCycle-2)*length(TimeMatrix)+1:(iCycle-1)*length(TimeMatrix))=real(ifft(ifftshift(FStress')))';

end

