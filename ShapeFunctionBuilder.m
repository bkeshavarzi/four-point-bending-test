function [N1,N2,N3,N4,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y,B4_x,B4_y]=ShapeFunctionBuilder()

N1=@(x,y) 0.25*(1-x)*(1-y);
N2=@(x,y) 0.25*(1+x)*(1-y);
N3=@(x,y) 0.25*(1+x)*(1+y);
N4=@(x,y) 0.25*(1-x)*(1+y);

B1_x=@(x,y) -1*0.25*(1-y);
B1_y=@(x,y) -1*0.25*(1-x);
B2_x=@(x,y) +1*0.25*(1-y);
B2_y=@(x,y) -1*0.25*(1+x);
B3_x=@(x,y) +1*0.25*(1+y);
B3_y=@(x,y) +1*0.25*(1+x);
B4_x=@(x,y) -1*0.25*(1+y);
B4_y=@(x,y) +1*0.25*(1-x);


end

