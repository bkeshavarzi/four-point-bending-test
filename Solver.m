function [U]=Solver(U,iTime,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,Ksm1,Ksm2,Km2m1,Km2m2,Km1m1,F_mu,F_mf,F_s)
%          U,iCycle,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,K_s_mf,K_mu_s',K_mu_mf,K_mu_mu,K_mf_mf,F_mu,F_mf,F_s
Km1m2=Km2m1';

KG=cat(2,Km2m2,Km2m1,zeros(length(MasterUsualDOF),length(SlaveDOF)));
KG=cat(1,KG,cat(2,Km1m2,Km1m1,-1*ones(length(MasterForceDOF),length(SlaveDOF))));
KG=cat(1,KG,cat(2,Ksm2,Ksm1,zeros(length(SlaveDOF),length(SlaveDOF))));

%size(KG)
%size(F)

FG=cat(1,F_mu,F_mf,F_s);

Sol=KG\FG;

UTry(1:length(MasterUsualDOF))=Sol(1:length(MasterUsualDOF));
UTry(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF))=Sol(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF));
UTry(length(MasterUsualDOF)+length(MasterForceDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF)+length(SlaveDOF))=-1*Epsilon0*70*10^-3*abs(sin(w0*(iTime-1)*dT));
UTry=UTry';
UTry=cat(1,UTry,zeros(length(BCVert)+length(BCHor),1));

[~,Index]=sort(cat(1,MasterUsualDOF,MasterForceDOF,SlaveDOF,BCVert,BCHor),'ascend');
UTry=UTry(Index);
U=cat(2,U(:,iTime-1),UTry);

end



