clc
figure

for ielem=1:size(Element,1)
    
    gaussid=4*(ielem-1)+1:4*(ielem-1)+4;
    gaussid=gaussid';
    ElementId=[Element(ielem,2);Element(ielem,3);Element(ielem,4);Element(ielem,5)];
    
    TempNode=zeros(4,3);
    TempNode(1,:)=Node(Element(ielem,2),:);
    TempNode(2,:)=Node(Element(ielem,3),:);
    TempNode(3,:)=Node(Element(ielem,4),:);
    TempNode(4,:)=Node(Element(ielem,5),:);
    
    Xc=mean(TempNode(:,2));a=0.5*(max(TempNode(:,2))-min(TempNode(:,2)));
    Yc=mean(TempNode(:,3));b=0.5*(max(TempNode(:,3))-min(TempNode(:,3)));
    
    xgp=[-1/sqrt(3) 1/sqrt(3) 1/sqrt(3) -1/sqrt(3)];
    ygp=[-1/sqrt(3) -1/sqrt(3) 1/sqrt(3) 1/sqrt(3)];
    
    x1=a*xgp(1)+Xc;x2=a*xgp(2)+Xc;x3=a*xgp(3)+Xc;x4=a*xgp(4)+Xc;
    y1=a*ygp(1)+Yc;y2=a*ygp(2)+Yc;y3=a*ygp(3)+Yc;y4=a*ygp(4)+Yc;
    
%     x=[x1 x2 x3 x4];
%     y=[y1 y2 y3 y4];

    x=[TempNode(1,2) TempNode(2,2) TempNode(3,2) TempNode(4,2)];
    y=[TempNode(1,3) TempNode(2,3) TempNode(3,3) TempNode(4,3)];
    cycle=362;
    patch(x,y,NodalC(ElementId,cycle),'EdgeColor','none')
    hold on
    
end

xlim([min(Node(:,2)) max(Node(:,2))])
colorbar
xlabel('X (m)')
ylabel('Y (m)')
title(sprintf('C at Cycle = %d',cycle))