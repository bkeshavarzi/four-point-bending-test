function [ ] = PlotMesh(Node,Element,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor)

close all
clc

for iNode=1:size(Node,1)
    
    hold on
    plot(Node(iNode,2),Node(iNode,3),'o');
    
end

for ielem=1:size(Element,1)
    
    for j=1:3
        
        plot([Node(Element(ielem,j+1),2) Node(Element(ielem,j+2),2)],[Node(Element(ielem,j+1),3) Node(Element(ielem,j+2),3)],'blue.-','markersize',15)
        hold on
        
    end
    
end

MasterForceNodes=floor(0.5*(MasterForceDOF+1));MasterForceNodes=unique(MasterForceNodes);
MasterUsualNodes=floor(0.5*(MasterUsualDOF+1));MasterUsualNodes=unique(MasterUsualNodes);
SlaveNodes=floor(0.5*(SlaveDOF+1));SlaveNodes=unique(SlaveNodes);
BCVertNodes=floor(0.5*(BCVert+1));BCVertNodes=unique(BCVertNodes);
BCHorNodes=floor(0.5*(1+BCHor));BCHorNodes=unique(BCHorNodes);

for inode=1:length(MasterForceNodes)
    
    plot(Node(MasterForceNodes(inode),2),Node(MasterForceNodes(inode),3),'green.-','markersize',25);
    
end


% 
for inode=1:length(BCVertNodes)
    
    plot(Node(BCVertNodes(inode),2),Node(BCVertNodes(inode),3),'red.-','markersize',25);
    
end

for inode=1:length(BCHorNodes)
    
    plot(Node(BCHorNodes(inode),2),Node(BCHorNodes(inode),3),'black.-','markersize',25);
    
end

for inode=1:length(SlaveNodes)
    
    plot(Node(SlaveNodes(inode),2),Node(SlaveNodes(inode),3),'yellow.-','markersize',25);
    
end
 

% index=find(Node(:,2)==0);
%
% for inode=1:length(index)
%
%     plot(Node(index(inode),2),Node(index(inode),3),'red.-','markersize',20);
%     hold on
%
% end
%
% index1=find(Node(:,2)==59.5);
% index2=find(Node(index,3)==0);
% index3=find(Node(index,3)==max(Node(:,3)));
% plot(Node(index1(index2),2),Node(index1(index2),3),'green.-','markersize',20);
% plot(Node(index1(index3),2),Node(index1(index3),3),'green.-','markersize',20);
%
% index1=find(Node(:,2)==178.5);
% index2=find(Node(index,3)==0);
% index3=find(Node(index,3)==max(Node(:,3)));
% plot(Node(index1(index2),2),Node(index1(index2),3),'black.-','markersize',20);
% plot(Node(index1(index3),2),Node(index1(index3),3),'black.-','markersize',20);


end

