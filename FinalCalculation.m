
% clear
% load('Result800VTa00_2.mat')
clc

F0=Force(197:201);
F0max=max(F0);

Factor=(3*0.357^2-4*0.119^2)/(12*0.05);
Delta=1*Amp*Factor;

Sigmat0=3*0.119*F0max/(0.063*0.05^2);
Epsilont0=12*Delta*0.05/(3*0.357^2-4*0.0119^2);

S0=Sigmat0/Epsilont0;
N0=50;

for index=1:length(Point2Cycle2)
    
    
    
    if index==1
        
        findex(index)=202+(index-1)*length(w);
        
    else
        
        findex(index)=202+(index*length(w))-1;
        
    end
    
end

findex=findex';

len=length(findex);

for irow=1:len-2
    
    f(irow)=max(abs(Force(findex(irow):findex(irow+1))));
    
end

f=f';
Sigmat=3*0.119*f/(0.063*0.05^2);
St=(Sigmat/Epsilont0);
Nn=Point2Cycle2(1:end);

NM=(Nn.*St)/(S0*N0);
