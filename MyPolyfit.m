function [p] = MyPolyfit( x,y )

x=reshape(x,1,length(x));
y=reshape(y,1,length(y));

A=zeros(2,2);
A(1,1)=x*x';
A(1,2)=x*ones(length(x),1);
A(2,1)=A(1,2);
A(2,2)=length(x);

b=zeros(2,1);
b(1,1)=y*x';
b(2,1)=y*ones(length(x),1);

p=A\b;

end

