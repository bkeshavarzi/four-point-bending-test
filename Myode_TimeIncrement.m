function [ dydt ] = Myode_TimeIncrement(t,y,time,Vector1,Vector2,alpha,a,b)

nps=interp1(time,Vector1,t);
nps=nps.^(2*alpha);

dydt=Vector2.*(y.^(alpha*(b-1))).*exp(-1*a*y.^b).*nps';  


end

