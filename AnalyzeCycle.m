function [MaxSigma,MaxStrain,Stiffness,Phi]=AnalyzeCycle(TimeSol,TimeForce,Ut1,Ut2,CycleCriteria,iCycle,Cycle,w0)


a=118.5*10^-3;
b=64*10^-3;
h=54*10^-3;
L=3*118.5*10^-3;

if iCycle <= CycleCriteria
    
    MaxDisp=max(abs(Ut1));
    MaxForce=max(abs(TimeForce));
    Time1=TimeSol(find(MaxDisp==abs(Ut1)));
    Time2=TimeSol(find(MaxForce==max(abs(TimeForce))));
    
else
    
    MaxDisp=max(abs(Ut2));
    MaxForce=max(abs(TimeForce));
    Time1=TimeSol(find(MaxDisp==abs(Ut2)));
    Time2=TimeSol(find(MaxForce==max(abs(TimeForce))));
    
end
    
MaxForce=max(abs(TimeForce));
MaxSigma=3*a*MaxForce*1000/(b*h^2);
MaxStrain=12*MaxDisp*h/((3*L^2)-(4*a^2));
Stiffness=MaxSigma/MaxStrain;
Phi=360*w0*(Time2-Time1);

end

