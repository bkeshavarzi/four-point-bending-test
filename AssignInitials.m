function [v,E,Amp,w0]=AssignInitials()
 

w0=10;         %Imposed dispalcemnet's frequency, Hz
Amp=900*10^-6; % Amplitude of imposed displacement

v=0.35;        %Poisson ratio
E=200e9;       %Elastic Modulus

end

