function [Element,CHistory,WRC,Integ1_C]=UpdateDamageTimeIncrement(Element,Point2Cycle1,NOCycle,RTime,iTime,a,b,Gamma,Delta,alpha,Dr,Direction,CHistory,Stress,WRC,Integ1_C)

index=find(Point2Cycle1==iTime);
Nend=NOCycle(index);
TimeDomain=RTime(iTime-4:iTime);
N=[1:Nend];

Vec1=[1;0];
Vec2=cat(1,Direction(:,1)',Direction(:,2)');
NormVec2=sqrt(sum(Vec2.^2,1));
Teta=acos(sum(bsxfun(@times,Vec1,Vec2),1)./NormVec2); 

Vec1=cos(Teta).^2;
Vec2=sin(Teta).^2;
Vec3=sin(2*Teta);

TransformationMatrix=reshape(cat(1,Vec1,Vec2,Vec3),12*size(Element,1),1);
TStress=bsxfun(@times,TransformationMatrix,Stress(:,iTime-4:iTime));
PStress=zeros(4*size(Element,1),5);

for it=1:5
    
    PStress(:,it)=sum(reshape(TStress(:,it),3,4*size(Element,1)),1)';
    
end
      
PStress(PStress<0)=0;
Vector1=bsxfun(@times,(1./reshape(Element(:,6:9)',4*size(Element,1),1)),PStress)'; % row== # of points in time, col== # of gpts
max(max(Vector1))
% Vector1=(Vector1).^(2*alpha);
max(max(Vector1))
Sint=((-1/a)*log(Element(:,6:9))).^(1/b);
Sint=reshape(Sint',4*size(Element,1),1)';

Factor=(0.5*a*b)^alpha;

[~,S]=ode45(@(t,y) Myode_TimeIncrement(t,y,TimeDomain,Vector1,Factor,alpha,a,b),TimeDomain,Sint);
S=real(S);

Send=S(end,:)';
Cend=exp(-1*a*Send.^b);
CHistory(:,Nend)=Cend;

CH_GP=1-CHistory(:,1:Nend);
dN=diff(N);
dN=repmat(dN,4*size(Element,1),1);

Summation=cumsum(0.5*(CH_GP(:,1:end-1).*dN+CH_GP(:,2:end).*dN),2);
C=reshape(Element(:,6:9)',4*size(Element,1),1);

C(Summation >= Dr*N(end))=0.000001;
C=reshape(C,size(Element,1),4);
Element(:,6:9)=C;

end










