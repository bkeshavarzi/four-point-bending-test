function [Node,Element]=MeshStructure()


x1=linspace(0,59.5,6);
x2=linspace(59.5,178.5,10);x2(1)=[];
x3=linspace(178.5,190,3);x3(1)=[];

x=cat(2,x1,x2,x3);
y1=linspace(0,25,5);
y2=linspace(25,50,5); y2(1)=[];
y=cat(2,y1,y2);

nx=length(x);
ny=length(y);

Node=zeros(nx*ny,3);
Node(:,1)=linspace(1,nx*ny,nx*ny);
Node(:,2)=repmat(x',ny,1);
Node(:,3)=reshape(bsxfun(@plus,zeros(1,nx),linspace(0,max(y),ny)')',nx*ny,1);

Node(:,2)=Node(:,2)/1000;
Node(:,3)=Node(:,3)/1000;

ID=bsxfun(@plus,linspace(1,nx,nx),(0:nx:nx*(ny-1))');

Element=zeros((nx-1)*(ny-1),9);
Element(:,1)=(1:(nx-1)*(ny-1))';
Element(:,2)=reshape(ID(1:end-1,1:end-1)',(nx-1)*(ny-1),1);
Element(:,3)=reshape(ID(1:end-1,2:end)',(nx-1)*(ny-1),1);
Element(:,4)=reshape(ID(2:end,2:end)',(nx-1)*(ny-1),1);
Element(:,5)=reshape(ID(2:end,1:end-1)',(nx-1)*(ny-1),1);

end

