function [ dydt ] = Myode1(t,y,Time,ps,a,b,alpha)

    nps=interp1(Time,ps,t);
    dydt=((0.5*a*b*(nps.^2)).^alpha).*((y.^(b-1)).*exp(-1*a*y.^b)).^(alpha);
    
end

