function [Cycle,w,SelectedW,TimeMatrix,Ut,Uw,USelectedW] = FrequencyDomainCycle(w0,Amp)


FirstCycle=50;
CycleIncrement=100;

CycleEnd=5e4;
Cycle=(FirstCycle:CycleIncrement:CycleEnd+CycleIncrement)';

ShiftFactor=1;

TimeMatrix=(0:(1/6)*(1/w0):CycleIncrement/w0)';    % It means 100 steps for each damage updating stage
 
Factor=(3*0.357^2-4*0.119^2)/(12*0.05);
Ut=-1*Amp*Factor*abs(sin(pi*w0*TimeMatrix));

dw=2*pi/max(TimeMatrix);
nt=length(TimeMatrix);
nt2=floor(nt/2);
w = (-nt2:-nt2+nt-1)'*dw*ShiftFactor;
Uw = fftshift(fft(Ut));
ws=(0:0.1:7)';
SelectedW=zeros(length(ws),1);
last_member=dw;
i=-1;

for iw=1:length(ws)
    
    i=i+1;
    SelectedW(iw)=last_member*2^i;
    last_member=last_member*2^i;
    
end

SelectedW=cat(1,-1*SelectedW(end:-1:1),0,SelectedW);

index=find(SelectedW>max(w),1,'first');
SelectedW(index:end)=[];
index=find(SelectedW<min(w),1,'last');
SelectedW(1:index)=[];

if SelectedW(end) < max(w)
    
    SelectedW=cat(1,SelectedW,max(w));

end

if SelectedW(1) > min(w)
    
    SelectedW=cat(1,min(w),SelectedW);
    
end

USelectedW=interp1(w,Uw,SelectedW,'pchip');
