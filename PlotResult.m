% close all
clc

XP=zeros(4,size(Element,1));
YP=zeros(4,size(Element,1));

Xgpt=zeros(4,size(Element,1));
Ygpt=zeros(4,size(Element,1));

UXP=zeros(4,size(Element,1));
UYP=zeros(4,size(Element,1));

Sxx=zeros(size(Element,1),4);
Syy=zeros(size(Element,1),4);
Sxy=zeros(size(Element,1),4);

C=zeros(size(Element,1),4);
MaxPstrain_StressBased=zeros(size(Element,1),4);

XCor=zeros(4*size(Element,1),1);
YCor=zeros(4*size(Element,1),1);
Ud=zeros(4*size(Element,1),1);
Vd=zeros(4*size(Element,1),1);

GPT_x=[-1/sqrt(3)   1/sqrt(3)  1/sqrt(3)  -1/sqrt(3)];
GPT_y=[-1/sqrt(3)  -1/sqrt(3)  1/sqrt(3)   1/sqrt(3)];


it =2;

for ielem=1:size(Element,1)
    
    XP(1,ielem)=Node(Element(ielem,2),2);
    XP(2,ielem)=Node(Element(ielem,3),2);
    XP(3,ielem)=Node(Element(ielem,4),2);
    XP(4,ielem)=Node(Element(ielem,5),2);
    
    YP(1,ielem)=Node(Element(ielem,2),3);
    YP(2,ielem)=Node(Element(ielem,3),3);
    YP(3,ielem)=Node(Element(ielem,4),3);
    YP(4,ielem)=Node(Element(ielem,5),3);
    
    UXP(1,ielem)=U(2*Node(Element(ielem,2),1)-1,it)*1000;
    UXP(2,ielem)=U(2*Node(Element(ielem,3),1)-1,it)*1000;
    UXP(3,ielem)=U(2*Node(Element(ielem,4),1)-1,it)*1000;
    UXP(4,ielem)=U(2*Node(Element(ielem,5),1)-1,it)*1000;
    
    UYP(1,ielem)=U(2*Node(Element(ielem,2),1),it)*1000;
    UYP(2,ielem)=U(2*Node(Element(ielem,3),1),it)*1000;
    UYP(3,ielem)=U(2*Node(Element(ielem,4),1),it)*1000;
    UYP(4,ielem)=U(2*Node(Element(ielem,5),1),it)*1000;
    
    XcElem=mean(XP(:,ielem));
    YcElem=mean(YP(:,ielem));
    a=0.5*(max(XP(:,ielem))-min(XP(:,ielem)));
    b=0.5*(max(YP(:,ielem))-min(YP(:,ielem)));
    
    for igpt=1:4
        
        GaussIdIndex=12*(ielem-1)+3*(igpt-1)+1:12*(ielem-1)+3*(igpt-1)+3;
        GaussId=4*(ielem-1)+igpt;
        
        Sxx(ielem,igpt)=StressTimeIncrementAnalysis(GaussIdIndex(1),it);
        Syy(ielem,igpt)=StressTimeIncrementAnalysis(GaussIdIndex(2),it);
        Sxy(ielem,igpt)=StressTimeIncrementAnalysis(GaussIdIndex(3),it);
        
        sigma=[Sxx(ielem,igpt) Sxy(ielem,igpt);Sxy(ielem,igpt) Syy(ielem,igpt)];
        [~,d]=eig(sigma);
        d=diag(d);
        MaxPstrain_StressBased(ielem,igpt)=max(d);
        
        Xgpt(igpt,ielem)=a*GPT_x(igpt)+XcElem;
        Ygpt(igpt,ielem)=b*GPT_y(igpt)+YcElem;
        
%         C(ielem,igpt)=CHistory(GaussId,it);
        
        XCor(GaussId,1)=a*GPT_x(igpt)+XcElem;
        YCor(GaussId,1)=b*GPT_y(igpt)+YcElem;
        Ud(GaussId,1)=Direction(GaussId,1);
        Vd(GaussId,1)=Direction(GaussId,2);
        
    end

end

figure
title('Ux')
patch(XP,YP,UXP,'LineStyle','none')
colorbar
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off

figure
title('Uy')
patch(XP,YP,UYP,'LineStyle','none')
colorbar
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off

figure
title('Sxx')
patch(XP,YP,Sxx','LineStyle','none')
colorbar
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off
% 
figure
title('Syy')
patch(XP,YP,Syy','LineStyle','none')
colorbar
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off

figure
title('Sxy')
patch(XP,YP,Sxy','LineStyle','none')
colorbar
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off

figure
title('Direction')
quiver(XCor,YCor,Ud,Vd);
axis equal
xlim([min(Node(:,2))  max(Node(:,2))])
ylim([min(Node(:,3))  max(Node(:,3))])
axis off

% 
% figure
% title('Sxy')
% patch(XP,YP,Sxy','LineStyle','none')
% colorbar
% axis equal
% xlim([min(Node(:,2))  max(Node(:,2))])
% ylim([min(Node(:,3))  max(Node(:,3))])
% axis off
% 
% figure
% title('C')
% patch(XP,YP,C','LineStyle','none')
% colorbar
% axis equal
% xlim([min(Node(:,2))  max(Node(:,2))])
% ylim([min(Node(:,3))  max(Node(:,3))])
% axis off

%
% figure
% title('StressBased')
% patch(XP,YP,MaxPstrain_StressBased','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% axis off
%
% figure
% title('InverseStressBased')
% patch(XP,YP,MaxPstrain_InverseStressBased','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% axis off

% figure
% title('C')
% patch(XP,YP,C','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% caxis([0 1])
% title('C','FontSize',18);
% colorbar('peer',gca,'FontSize',16);
% axis off
% 
% figure
% title('Failed Element')
% patch(XP,YP,FailureElement','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% caxis([0 1])
% title('Failed Element','FontSize',18);
% colorbar('peer',gca,'FontSize',16);
% axis off