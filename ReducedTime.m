function [RTime,dT]=ReducedTime(Time,T,ShiftFactor)

RTime=Time/ShiftFactor;
df=diff(Time);
dT=df(1)/ShiftFactor;

end
