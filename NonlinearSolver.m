function [UTemp,Element]=NonlinearSolver(Node,Element,U,Direction,RTime,iTime,Einf,Rho,E,v,PForceSeries,PForceInf,PForceGaussSeries,PForceGaussInf,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,Ksm1,Ksm2,Km2m1,Km2m2,Km1m1,F_mu,F_mf,F_s,a,b,alpha,Epsilon0,w0,dT)
%    Node,Elemnet,U,Direction,RTime,iTime,Einf,Rho,E,v,PForceSeries,PForceInf,PForceGaussSeries,PForceGaussInf,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,K_s_mf,K_mu_s',K_mu_mf,K_mu_mu,K_mf_mf,F,a,b,alpha

IterCount=0;
Tol=1e-05;
Km1m2=Km2m1';
UTemp=U;

KG=cat(2,Km2m2,Km2m1,zeros(length(MasterUsualDOF),length(SlaveDOF)));
KG=cat(1,KG,cat(2,Km1m2,Km1m1,-1*ones(length(MasterForceDOF),length(SlaveDOF))));
KG=cat(1,KG,cat(2,Ksm2,Ksm1,zeros(length(SlaveDOF),length(SlaveDOF))));

%size(KG)
%size(F)

FG=cat(1,F_mu,F_mf,F_s);

Sol=KG\FG;

UTry(1:length(MasterUsualDOF))=Sol(1:length(MasterUsualDOF));
UTry(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF))=Sol(length(MasterUsualDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF));
UTry(length(MasterUsualDOF)+length(MasterForceDOF)+1:length(MasterUsualDOF)+length(MasterForceDOF)+length(SlaveDOF))=-1*Epsilon0*70*10^-3*abs(sin(w0*(iTime-1)*dT));
UTry=UTry';
UTry=cat(1,UTry,zeros(length(BCVert)+length(BCHor),1));

[~,Index]=sort(cat(1,MasterUsualDOF,MasterForceDOF,SlaveDOF,BCVert,BCHor),'ascend');
UTry=UTry(Index);
UIter=cat(2,U(:,iTime-1),UTry);

TempElement=Element;
TempDirection=Direction;

OldGaussSeries=PForceGaussSeries;
OldGaussInf=PForceGaussInf;

while and (IterCount <20,Tol>=1e-05)
    
    IterCount=IterCount+1;
    [DUsol,PForceGaussSeries,PForceGaussInf]=IncrementalDisplacement(Node,TempElement,Element,UIter,MasterForceDOF,MasterUsualDOF,SlaveDOF,PForceSeries,PForceInf,PForceGaussSeries,PForceGaussInf,TempDirection,RTime,iTime,E,Rho,Einf,v);
    
        
    Tol=norm(DUsol,2);  % Think about Convergence Criteria / Ok
    
    if Tol >1e-05
        
        UIter=cat(2,U(:,iTime-1),UIter(:,2)+DUsol);
        [TempElement]=DamageUpdating(Node,Element,iTime,UIter,PForceGaussSeries,PForceGaussInf,OldGaussSeries,OldGaussInf,E,Rho,Einf,v,a,b,alpha);
        
        
        if  IterCount > 20
            
            disp(sprintf('Reach to convergency for Segment %d, Tol is %8.6f \n',iTime,Tol));
            Tol;
            
        end
        
    else
        
        disp(sprintf('Reach to convergency for Segment %d after %d Iterations, Tol is %8.6f \n',iTime,IterCount,tol));
        UTemp(:,iTime)=UIter(:,2)+DUsol;
        
        break
        
    end
    
end

Element=TempElement;


end




