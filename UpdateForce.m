function [PForceSeries,PForceInf,GaussStress,NodalStress,PStressSeries,PStressInf,PStressGaussSeries,PStressGaussInf]=UpdateForce(Node,Element,iTime,U,PForceSeries,PForceInf,PStressGaussSeries,PStressGaussInf,PStressSeries,PStressInf,E,Rho,Einf,v)

D=(1/((1+v)*(1-2*v)))*[1-v v 0;v 1-v 0;0 0 1-2*v]; % Plain Strain

GPT_x=[-1/sqrt(3)   1/sqrt(3)  1/sqrt(3)  -1/sqrt(3)];
GPT_y=[-1/sqrt(3)  -1/sqrt(3)  1/sqrt(3)   1/sqrt(3)];
WGPT=[1 1 1 1];

TimeStart=RTime(iTime-1);
TimeEnd=RTime(iTime);
TimeMiddle=0.5*(TimeStart+TimeEnd);


[~,~,~,~,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y,B4_x,B4_y]=ShapeFunctionBuilder();

StressSeries=zeros(3*size(Node,1),size(Rho,1));
StressInf=zeros(3*size(Node,1),1);
StressGaussSeries=zeros(12*size(Element,1),size(Rho,1));
StressGaussInf=zeros(12*size(Element,1),1);

ForceSeries=zeros(2*size(Node,1),size(Rho,1)); % This Matrix Stores force for each Elemnt in current time strep due to viscous Effect_ all the prony is stored
ForceInf=zeros(2*size(Node,1),1);              % This Matrix Stores force for each Elemnt in current time strep due to viscous Effect_ Einf is stored

for ielem=1:size(Element,1)
    
    ElementId=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4);2*Element(ielem,5)-1;2*Element(ielem,5)];
    
    TempNode=zeros(4,3);
    TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
    TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
    TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
    TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
    
    a=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
    b=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
    
    
    for igpt=1:4
        
        GaussIdIndex=12*(ielem-1)+3*(igpt-1)+1:12*(ielem-1)+3*(igpt-1)+3;
        GaussId=4*(ielem-1)+igpt;
        
        C0=Element(ielem,igpt+5);
        
        % check A formulation
        
        A11=(1/9)*(C0+1*(2*(1+v)/(1-2*v)));
        A22=C0+0.5*((1-2*v)/(1+v));
        A12=(1/3)*(C0-1);
        A44=0.5/(1+v);
        A66=A44;
        
        C=zeros(3);
        
        if Direction(GaussId,2)==1
            
            % It means X_bar is direction of Maximum Principal Pseudo
            % Strain
            C(1,1)=A11+(4/3)*A12+(4/9)*A22;
            C(1,2)=A11+(1/3)*A12-(2/9)*A22;
            C(1,3)=0;
            C(2,1)=C(1,2);
            C(2,2)=A11-(2/3)*A12+(1/9)*A22+A66;
            C(2,3)=0;
            C(3,1)=0;
            C(3,2)=0;
            C(3,3)=A44;
            
        else
            
            % It means Y_bar is direction of Maximum Principal Pseudo
            % Strain
            C(1,1)=A11-(2/3)*A12+(1/9)*A22+A66;
            C(1,2)=A11+(1/3)*A12-(2/9)*A22;
            C(1,3)=0;
            C(2,1)=C(1,2);
            C(2,2)=A11+(4/3)*A12+(4/9)*A22;
            C(2,3)=0;
            C(3,1)=0;
            C(3,2)=0;
            C(3,3)=A44;
            
        end
        
        dN1_x=feval(B1_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN1_y=feval(B1_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN2_x=feval(B2_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN2_y=feval(B2_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN3_x=feval(B3_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN3_y=feval(B3_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        dN4_x=feval(B4_x,GPT_x(igpt),GPT_y(igpt))*(1/a);
        dN4_y=feval(B4_y,GPT_x(igpt),GPT_y(igpt))*(1/b);
        
        B=zeros(3,8);
        
        B(1,1)=dN1_x;B(2,2)=dN1_y;B(3,1)=dN1_y;B(3,2)=dN1_x;
        B(1,3)=dN2_x;B(2,4)=dN2_y;B(3,3)=dN2_y;B(3,4)=dN2_x;
        B(1,5)=dN3_x;B(2,6)=dN3_y;B(3,5)=dN3_y;B(3,6)=dN3_x;
        B(1,7)=dN4_x;B(2,8)=dN4_y;B(3,7)=dN4_y;B(3,8)=dN4_x;
        
        TTeta=-1*Direction(GaussId,1);
        
        T=[cos(TTeta)^2 sin(TTeta)^2 -2*sin(TTeta)*cos(TTeta);sin(TTeta)^2 cos(TTeta)^2 2*sin(TTeta)*cos(TTeta);sin(TTeta)*cos(TTeta) -1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2]; % Check this ????
        
        CBar=inv(T)*C*T;  % Check this ????/Ok
        
        
        for iprony=1:size(Rho,1)
            
            ForceSeries(ElementId,iprony)=ForceSeries(ElementId,iprony)+B'*CBar*D*EpronyCalculationPart(TimeEnd,TimeMiddle,Rho(iprony),E(iprony))*B*WGPT(igpt)*U(ElementId,iTime)*a*b;
            StressSeries(ElementId,iprony)=StressSeries(ElementId,iprony)+1*D*EpronyCalculationPart(TimeEnd,TimeMiddle,Rho(iprony),E(iprony))*B*U(ElementId,iTime);
            StressGaussSeries(GaussIdIndex,iprony)=1*D*EpronyCalculationPart(TimeEnd,TimeMiddle,Rho(iprony),E(iprony))*B*U(ElementId,iTime);
            
        end
        
        ForceInf(ElementId,1)=ForceInf(ElementId,1)+B'*CBar*D*Einf*B*WGPT(igpt)*U(ElementId,iTime)*a*b;
        StressInf(ElementId,1)=StressInf(ElementId,1)+1*D*Einf*B*U(ElementId,iTime);
        StressGaussInf(GaussIdIndex,1)=1*D*Einf*B*U(ElementId,iTime);
        
    end
    
end

PForceSeries=PForceSeries+StressSeries;
PForceInf=PForceInf+StressInf;

PStressSeries=PStressSeries+StressSeries;
PStressInf=PStressInf+StressInf;
PStressGaussSeries=PStressGaussSeries+StressGaussSeries;
PStressGaussInf=PStressGaussInf+StressGaussInf;

GaussStress(:,iTime)=sum(PStressGaussSeries,2)+PStressGaussInf;
NodalStress(:,iTime)=sum(PStressSeries,2)+PStressInf;

