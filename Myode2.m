function [ dydt ] = Myode2(t,y,b,alpha,DeltaC,C0)

%   nps=interp1(Time,ps,t);
%   dydt=((0.5*a*b*(nps.^2)).^alpha).*((y.^(b-1)).*exp(-1*a*y.^b)).^(alpha);

    eq = @(n,c) dc.*(c./c0).^(1+alpha).*(log(c)./log(c0)).^((1+alpha)*(1-1/b));
    dydt=(DeltaC).*((y/C0).^(1+alpha)).*((log(y)./log(C0)).^(1+alpha)*(1-(1/b)));
    
end

