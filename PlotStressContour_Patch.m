function [ ] = PlotStressContour_Patch(Node,Element,NodalSigmaXX,NodalSigmaYY,NodalSigmaXY,CI,it)


Id=(1:12)';



GPT_x=[-1/sqrt(3)   1/sqrt(3)  1/sqrt(3)  -1/sqrt(3)];
GPT_y=[-1/sqrt(3)  -1/sqrt(3)  1/sqrt(3)   1/sqrt(3)];

Xm=zeros(4*size(Element,1),1);
Ym=zeros(4*size(Element,1),1);
Cxx=zeros(4*size(Element,1),1);
Cyy=zeros(4*size(Element,1),1);
Cxy=zeros(4*size(Element,1),1);

for ielem=1:size(Element,1)
    
    
    ElementId=4*(ielem-1)+1:4*(ielem-1)+4;
    
    GaussIdIndex=12*(ielem-1)+Id;
    IdX=GaussIdIndex(1:3:end-2);
    IdY=GaussIdIndex(2:3:end-1);
    IdXY=GaussIdIndex(3:3:end);
   
    TempNode=zeros(4,3);
    TempNode(1,:)=Node(find(Node(:,1)==Element(ielem,2)),:);
    TempNode(2,:)=Node(find(Node(:,1)==Element(ielem,3)),:);
    TempNode(3,:)=Node(find(Node(:,1)==Element(ielem,4)),:);
    TempNode(4,:)=Node(find(Node(:,1)==Element(ielem,5)),:);
    
    SigmaXX=NodalSigmaXX(TempNode(:,1),it);
    SigmaYY=NodalSigmaYY(TempNode(:,1),it);
    SigmaXY=NodalSigmaXY(TempNode(:,1),it);
    CM=CI(TempNode(:,1),it);
    
    Xc=mean(Node(Element(ielem,2:5),2));
    Yc=mean(Node(Element(ielem,2:5),3));
    
    aa=0.5*max(abs(TempNode(:,2)-TempNode(1,2)*ones(4,1)));
    bb=0.5*max(abs(TempNode(:,3)-TempNode(1,3)*ones(4,1)));
    
    Xv=aa*GPT_x+Xc;Xv=reshape(Xv,length(Xv),1);
    Yv=bb*GPT_y+Yc;Yv=reshape(Yv,length(Yv),1);
    
    Xm(ElementId,1)=TempNode(:,2);
    Ym(ElementId,1)=TempNode(:,3);
    
    
    Cxx(ElementId,1)=SigmaXX;
    Cyy(ElementId,1)=SigmaYY;
    Cxy(ElementId,1)=SigmaXY;
    CM(ElementId,1)=CM;

    patch(TempNode(:,2),TempNode(:,3),SigmaXX,'EdgeColor','flat')
    hold on
    
end

colorbar
xlim([min(Node(:,2)) max(Node(:,2))])
xlabel('X (m)')
ylabel('Y (m)')
title('Sigma XX (kPa)')




