function [ dydt ] = myfun(t,y,time,ft,factor)

dydt=zeros(2,1);
nps=interp1(time,ft,t);
dydt=nps;
dydt=factor'.*dydt;
dydt=reshape(dydt,max(size(dydt)),1);
% dydt(1)=nps(1);
% dydt(2)=nps(2);

end

