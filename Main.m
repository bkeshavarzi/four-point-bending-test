clear
clc

% Beam dimensions

L=380; %length of the specimen
H=50;  %height of the specimen
t=63;  %width of the specimen

%*************************************************************************
%****************  Assign Initial Parametrs ******************************

[v,E,Amp,w0]=AssignInitials();

%****************  Constructing Structural Mesh **************************

[Node,Element]=MeshStructure();

%****************  Defining Specific Nodes   *****************************

[MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor]=InitializeBlocking(Node,Element);

%***************** Defining time domain for first part  *****************

TimeIncrementDomain=(0:5)';  %Total Number of Cycles=5*10^5, Num of points per cycle are 5
npt=5; % # of points per cycle

[Set,Point2Cycle1,NOCycle,Us,TimeDomainTime] = TimeIcrementDomain(TimeIncrementDomain,Amp,w0,npt);

%***************** Defining time domain for second part *****************

[Point2Cycle2,Freq,SelectedW,TimeMatrix,Ut,Uw,USelectedW] = FrequencyDomainCycle(w0,Amp);

%*********************  Defining Direction Properties **********************************************

Direction=zeros(4*size(Element,1),3);
Direction(:,1)=1;
Direction(:,2)=0;
Direction(:,3)=1;
DirectionChange=zeros(size(Element,1)*4,1);

%********************    Defining some matrix for displacemnt (U), stress result for first part and second part, and force***************************

U=zeros(2*size(Node,1),length(TimeDomainTime)+length(TimeMatrix)*(length(Point2Cycle2)-1));
StressTimeIncrementAnalysis=zeros(3*4*size(Element,1),length(TimeDomainTime));
StressCycleIncrementAnalysis=zeros(12*size(Element,1),length(TimeMatrix)*(length(Point2Cycle2)-1));
Force=zeros(length(TimeDomainTime)+length(TimeMatrix)*(length(Point2Cycle2)-1),1);

%*******************    finding displacement, stress, force and directin

for iTime=2:length(TimeDomainTime)
    
    iTime
    [U,StressTimeIncrementAnalysis,Force,Direction,DirectionChange]=GStiffnessMatrixTimeIncrement(Node,Element,t,U,Us,iTime,E,v,Force,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,StressTimeIncrementAnalysis,Direction,DirectionChange);
    
end

for iCycle=2:2%length(Point2Cycle2)
    
    [U,StressCycleIncrementAnalysis,Force]=GStiffnessMatrixCycleIncrement(Node,Element,t,iCycle,U,Direction,DirectionChange,TimeDomainTime,TimeMatrix,v,E,MasterForceDOF,MasterUsualDOF,SlaveDOF,BCVert,BCHor,Freq,SelectedW,USelectedW,Uw,Force,StressCycleIncrementAnalysis);
    
end




