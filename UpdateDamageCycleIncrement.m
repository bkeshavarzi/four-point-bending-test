function [Element,Direction,HDirection,CHistory,WRC,MaxPrincipalStress,Teta]=UpdateDamageCycleIncrement(Element,Point2Cycle1,Point2Cycle2,iCycle,w0,a,b,Gamma,Delta,Dr,alpha,Teta,Direction,HDirection,CHistory,TimeMatrix,w,MaxE33,Stress,WRC,MaxPrincipalStress,ShiftFactor)


NStart=Point2Cycle2(iCycle-1);
NEnd=Point2Cycle2(iCycle);

%dN=diff(Point2Cycle2);
dt=diff(TimeMatrix);
dt=dt(1)/ShiftFactor;

CycleTimeDomain_Start=(max(TimeDomainTime)+(icycle-2)*max(TimeMatrix))/ShiftFactor;
CycleTimeDomain_End=(max(TimeDomainTime)+(icycle-1)*max(TimeMatrix))/ShiftFactor;

TimeDomain=(CycleTimeDomain_Start:dt:CycleTimeDomain_End)';

sprintf('Updating damage for Cycle %d to Cycle %d,F*N is %d',NStart,NEnd,Fmax*NEnd)

% N=(NStart:NEnd);

StressCycle=Stress(:,(iCycle-2)*length(TimeMatrix)+1:(iCycle-1)*length(TimeMatrix));
Vec1=[1;0];
Vec2=cat(1,Direction(:,1)',Direction(:,2)');
NormVec2=sqrt(sum(Vec2.^2,1));
Teta=acos(sum(bsxfun(@times,Vec1,Vec2),1)./NormVec2); 
Vec1=cos(Teta).^2;
Vec2=sin(Teta).^2;
Vec3=sin(2*Teta);
TransformationMatrix=reshape(cat(1,Vec1,Vec2,Vec3),1,12*size(Element,1))';
TStress=bsxfun(@times,TransformationMatrix,StressCycle);
PStress=zeros(size(TStress,2),4*size(Element,1));

for it=1:size(StressCycle,2)
    
    PStress(:,igpt)=sum(reshape(TStress(:,it),3,4*size(Element,1)),1)';
    
end

PStress(PStress<0)=0;
Vector1=bsxfun(@times,(1./reshape(Element(:,6:9)',4*size(Element,1),1)),PStress)'; % row== # of points in time, col== # of gpts
% Vector1=(Vector1).^(2*alpha);


Sint=((-1/a)*log(Element(:,6:9))).^(1/b);
Sint=reshape(Sint',4*size(Element,1),1)'; 

Factor=(0.5*a*b)^alpha;

TSpan=[CycleTimeDomain_Start CycleTimeDomain_End];

[~,S]=ode45(@(t,y) Myode_TimeIncrement(t,y,TimeDomain,Vector1,Factor,alpha,a,b),TSpan,Sint);
S=real(S);
Send=reshape(S(end,:)',4,size(Element,1))';
Cend=exp(-1*a*Send.^b);

CHistory(:,length(NOCycle)+(icycle-1))=reshape(Cend',4*size(Element,1),1);

Cycles=cat(1,NOCycle,Point2Cylce2(icycle))';
dN=diff(Cycles);

CHG=1-CHistory(:,1:length(NOCycle)+(icycle-1));
CHG=CHG';
SummationDR_1=bsxfun(@times,dN,CHG(:,1:end-1));
SummationDR_2=bsxfun(@times,dN,CHG(:,2:end));
TempSummationDR=cumsum(0.5*(SummationDR_1+SummationDR_2),2);
SummationDR=TempSummationDR(:,end);
SummationDR=reshape(SummationDR,4,size(Element,1))';

Criterion=zeros(size(Element,1),4);
FailureDetection=Dr*Cycles(end);
Criterion(SummationDR>=FailureDetection)=1;

CElement=Element(:,6:9);
CElement(Criterion==1)=0.00001;

Element(:,6:9)=CElement;

end


