function EtPart=PronyIntegPart(Ei,Rhoi,Tm,Te)

RDelta=Te-Tm;

EtPart=Ei*exp(-1*RDelta/Rhoi);


end

