clc

n=size(Element24Stress,2);

for it=1:n
    
    Temp=Element24Stress(1:3,it);
    Temp=[Temp(1) Temp(3);Temp(3) Temp(2)];
    [v,d]=eig(Temp);
    
    if max(diag(d)) > 0
        
        indexx=find(max(diag(d))==diag(d));
        v(:,indexx)
        
    end
    
end