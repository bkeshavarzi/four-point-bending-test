function [Set1,Cycle,NCycle,Ut,TotalTime] = TimeIcrementDomain(Time,Amp,w0,npt)

FirstCycle=50;

Delta1=(1/w0);

Set1=(0:Delta1:FirstCycle*Delta1)';Set1(1)=[]; 
Cycle1=(0:length(Set1))';Cycle1(1)=[];NCycle1=(1:FirstCycle)';NCycle1(1)=[];

Cycle=Cycle1;
NCycle=NCycle1; 

Cycle=(npt-1)*Cycle+1; 

DeltaTime=Delta1/(npt-1);
TotalTime=(0:DeltaTime:max(Time))';

Factor=(3*0.357^2-4*0.119^2)/(12*0.05);
Ut=-1*Amp*Factor*abs(sin(pi*w0*TotalTime)); 

end

